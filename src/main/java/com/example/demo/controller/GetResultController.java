package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Result;
import com.example.demo.service.ResultService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("osushi")
public class GetResultController {

	@Autowired
	ResultService resultService;

	@CrossOrigin
	@PostMapping(value = "/getResult")
	public void getResult(@RequestBody String json) throws JsonMappingException, JsonProcessingException {
		System.out.println(json);

		// ObjectMapperのインスタンス生成
		ObjectMapper mapper = new ObjectMapper();

		// JsonNodeに変換。(これによりkeyを指定することでvalueを取得することができる。)
		JsonNode node = mapper.readTree(json);

		// get()で指定した文字列をkeyにvalueを取得する。
		int userId = node.get("userId").asInt();
		int driveScore = node.get("driveScore").asInt();
		int analyzeScore = node.get("analyzeScore").asInt();
		int createScore = node.get("createScore").asInt();
		int volunteerScore = node.get("volunteerScore").asInt();

		//配列を作成
		int[] array = {driveScore, analyzeScore, createScore, volunteerScore};


		//マップを作成(key:各タイプ、value:点数)
		Map<String, Integer> map = new HashMap<>();
		map.put("driveScore", driveScore );
		map.put("analyzeScore", analyzeScore);
		map.put("createScore", createScore);
		map.put("volunteerScore", volunteerScore);

//		String maxKey = null;
//		Integer maxValue = null;

//		//entrySetメソッドでマップのキーとValueを一つずつ取得
//		for (Map.Entry<String, Integer> entry : map.entrySet()) {
//			//最大値とValueを比較してValueが大きければ、そのときのキーとValueを代入
//			if (entry.getValue() > maxValue) {
//				maxKey = entry.getKey();
//				maxValue = entry.getValue();
//			}
//		}

		//スコアの最大値を求める(ここでは最大値のタイプは求めない)
		int maxScore = 0;
		for(int i = 1; i < array.length; i++) {
			if(maxScore < array[i]) {
				maxScore = array[i];
			}
		}

		//最大値を格納するためのMapを作成
		HashMap<String, Integer> maxScoreMap = new HashMap<String, Integer>();

		//entrySetメソッドでvalueが最大値と一致している場合Mapに格納
		for(HashMap.Entry<String, Integer> entry : map.entrySet()) {
			if(entry.getValue() == maxScore) {
				String maxKey = entry.getKey();
				int maxValue = entry.getValue();
				maxScoreMap.put(maxKey, maxValue);
			}
		}


		System.out.println(maxScoreMap);


		Result result = new Result();

		//最大値のFlgをtrueにする
		if(maxScoreMap.containsKey("driveScore")) {
			result.setDrive_flg(true);
		}
		if(maxScoreMap.containsKey("analyzeScore")) {
			result.setAnalyze_flg(true);
		}
		if(maxScoreMap.containsKey("createScore")) {
			result.setCreate_flg(true);
		}
		if(maxScoreMap.containsKey("volunteerScore")) {
			result.setVolunteer_flg(true);
		}

		result.setUserId(userId);
		result.setDrive_type(driveScore);
		result.setAnalyze_type(analyzeScore);
		result.setCreate_type(createScore);
		result.setVolunteer_type(volunteerScore);
		resultService.saveResult(result);
	}
}
