package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Department;
import com.example.demo.service.DepartmentService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("osushi")
public class SendDepartment {
	@Autowired
	DepartmentService departmentService;

	@CrossOrigin
	@GetMapping(value = "/sendDepartment")
	public ResponseEntity<List<Department>> sendResult() throws JsonProcessingException {

		List<Department> dbDepartment = departmentService.findAllDepartment();

		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(dbDepartment);
	}

}
