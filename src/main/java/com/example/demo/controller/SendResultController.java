package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Result;
import com.example.demo.service.ResultService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("osushi")
public class SendResultController {
	@Autowired
	ResultService resultService;

	@CrossOrigin
	@GetMapping(value = "/sendAllResult")
	public ResponseEntity<List<Result>> sendAllResult() throws JsonProcessingException {

		List<Result> dbResult = resultService.findAllResult();

		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(dbResult);
	}

	@CrossOrigin
	@GetMapping(value = "/sendResult/{id}")
	public ResponseEntity<List<Result>> sendResult(@PathVariable("id") int id) throws JsonProcessingException {


		List<Result> dbResultById = resultService.findResult(id);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(dbResultById);

	}
}
