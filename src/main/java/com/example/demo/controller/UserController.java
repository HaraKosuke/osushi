package com.example.demo.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("osushi")
public class UserController {

	@Autowired
	UserService userService;

	//ユーザー全取得
	@CrossOrigin
	@GetMapping(value = "/getUsers")
	public ResponseEntity<List<User>> sendUser() throws JsonProcessingException {

		List<User> dbUsers = userService.findAllUsers();
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(dbUsers);
	}

	//ログイン機能
	@CrossOrigin
	@PostMapping("/login")
	public ResponseEntity<User> login(@RequestBody String json) throws IOException, JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);

		String account = node.get("account").textValue();
		String password = node.get("password").textValue();

		User loginUser = userService.findLoginUser(account, password);
		loginUser.setPassword(null);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(loginUser);
	}

	//自身のパスワード変更
	@CrossOrigin
	@PostMapping("/edit_pass")
	public void editPass(@RequestBody String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);

		//jsonから値を取得
		Integer userId = node.get("id").asInt();
		String password = node.get("password").textValue();

		//IDから変更ユーザーのインスタンスを取得
		User user = userService.findById(userId);

		//変更情報をセット（上書き）
		user.setPassword(password);

		//変更情報をDBに保存
		userService.saveUser(user);

	}

	//ユーザー登録（管理者のみ）
	@CrossOrigin
	@PostMapping(value = "/addUser")
	public void getUser(@RequestBody String json) throws JsonMappingException, JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);

		//jsonから値を取得
		String account = node.get("account").textValue();
		Boolean adminFlg = node.get("adminFlg").asBoolean();
		String name = node.get("name").textValue();
		String password = node.get("password").textValue();
		Integer departmentId = node.get("departmentId").asInt();

		//Userにセット
		User user = new User();
		user.setAccount(account);
		user.setAdminFlg(adminFlg);
		user.setName(name);
		user.setPassword(password);
		user.setDepartmentId(departmentId);

		userService.saveUser(user);
	}

	//ユーザー情報変更（管理者のみ）
	@CrossOrigin
	@PostMapping("/edit")
	public void editUser(@RequestBody String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);
		Boolean adminFlg = null;
		String name = null;
		String password = null;
		Integer departmentId = null;

		//変更情報を取得
		Integer userId = node.get("id").asInt();
		if (node.get("adminFlg") != null) {
			adminFlg = node.get("adminFlg").asBoolean();
		}
		if (!node.get("name").asText().isEmpty()) {
			name = node.get("name").textValue();
		}
		if (!node.get("password").asText().isEmpty()) {
			System.out.println(node.get("password").asText());
			password = node.get("password").asText();
		}
		if (node.get("departmentId") != null) {
			departmentId = node.get("departmentId").asInt();
		}

		//IDから変更ユーザーのインスタンスを作成
		User user = userService.findById(userId);

		//変更情報をセット（上書き）
		if (adminFlg != null) {
			user.setAdminFlg(adminFlg);
		}
		if (name != null&&!name.isEmpty()) {
			user.setName(name);
		}
		if (password != null&&!password.isEmpty()) {
			user.setPassword(password);
		}
		if (departmentId != null) {
			user.setDepartmentId(departmentId);
		}

		//変更情報をDBに保存
		userService.saveUser(user);;
	}

	//編集するユーザーの情報を取得(管理者ページ)
	@CrossOrigin
	@GetMapping(value = "/sendUser/{id}")
	public ResponseEntity<User> sendResult(@PathVariable("id") int id) throws JsonProcessingException {

		User dbUserById = userService.findUserById(id);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(dbUserById);

	}

}
