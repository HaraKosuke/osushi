package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Question;
import com.example.demo.service.QuestionService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("osushi")
public class QuestionController {

	@Autowired
	QuestionService questionService;

	@CrossOrigin
	@GetMapping(value = "/sendQuestion")
	public ResponseEntity<List<Question>> sendQuestion() throws JsonProcessingException {

		List<Question> dbQuestion = questionService.findAllQuestions();

		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(dbQuestion);
	}
}
