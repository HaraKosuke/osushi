package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Configuration;

@Configuration
@Table(name = "results")
@Entity
public class Result {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="user_id")
	private int userId;

	@Column(name = "drive_type")
	private int drive_type;

	@Column(name = "analyze_type")
	private int analyze_type;

	@Column(name = "create_type")
	private int create_type;

	@Column(name = "volunteer_type")
	private int volunteer_type;

	@Column(name = "drive_flg")
	private boolean drive_flg;

	@Column(name = "analyze_flg")
	private boolean analyze_flg;

	@Column(name = "create_flg")
	private boolean create_flg;

	@Column(name = "volunteer_flg")
	private boolean volunteer_flg;

	@Column(name = "created_date", insertable = false, updatable = false)
	private Date createdDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getDrive_type() {
		return drive_type;
	}

	public void setDrive_type(int drive_type) {
		this.drive_type = drive_type;
	}

	public int getAnalyze_type() {
		return analyze_type;
	}

	public void setAnalyze_type(int analyze_type) {
		this.analyze_type = analyze_type;
	}

	public int getCreate_type() {
		return create_type;
	}

	public void setCreate_type(int create_type) {
		this.create_type = create_type;
	}

	public int getVolunteer_type() {
		return volunteer_type;
	}

	public void setVolunteer_type(int volunteer_type) {
		this.volunteer_type = volunteer_type;
	}

	public boolean isDrive_flg() {
		return drive_flg;
	}

	public void setDrive_flg(boolean drive_flg) {
		this.drive_flg = drive_flg;
	}

	public boolean isAnalyze_flg() {
		return analyze_flg;
	}

	public void setAnalyze_flg(boolean analyze_flg) {
		this.analyze_flg = analyze_flg;
	}

	public boolean isCreate_flg() {
		return create_flg;
	}

	public void setCreate_flg(boolean create_flg) {
		this.create_flg = create_flg;
	}

	public boolean isVolunteer_flg() {
		return volunteer_flg;
	}

	public void setVolunteer_flg(boolean volunteer_flg) {
		this.volunteer_flg = volunteer_flg;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


}
