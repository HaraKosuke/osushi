package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	//ログイン機能
	public User findLoginUser(String account, String password) {
		return userRepository.findByAccountAndPassword(account, password);
	}


	// ユーザー全取得
	public List<User> findAllUsers(){
		return userRepository.findAll();
	}


	//ユーザー登録(管理者)
	public void saveUser(User user) {
		userRepository.save(user);
	}


	//ユーザー編集
	public void editUser(User user) {
		userRepository.save(user);
	}

	//自身のパスワード変更
	public User findById(Integer id) {
		return userRepository.findById(id).orElse(null);
	}

	//ユーザーIDでそのユーザーのインスタンス取得
	public User findUserById(Integer id) {
		return userRepository.findById(id).orElse(null);
	}


}

