package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Result;
import com.example.demo.repository.ResultRepository;

@Service
public class ResultService {

	@Autowired
	ResultRepository resultRepository;

	// レコード全件降順で取得
	public List<Result> findAllResult() {
		return resultRepository.findAllByOrderByCreatedDateDesc();
	}

	//userIdの結果を取得
	public List<Result> findResult(Integer id) {
		return resultRepository.findByUserIdOrderByCreatedDateDesc(id);
	}

	public void saveResult(Result result) {
		resultRepository.save(result);
	}

}
